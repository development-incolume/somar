import sys
import pytest
from collections import Iterable


def somar(*args):
    lista = list()
    try:
        if len(args) == 1 and isinstance(args[0], Iterable):
            for i in range(len(args[0])):
                lista.append(float(args[0][i]))
            pass
        else:
            for i in range(len(args)):
                lista.append(float(args[i]))
                #print(args[i])
            #print(args)
        return sum(lista)
    except ValueError:
        raise
    except:
        print(sys.exc_info())



def test_num():
    assert somar() == 0
    assert somar(1, 2) == 3
    assert somar(0, 0) == 0
    assert somar(0, 1) == 1
    assert somar(-10, 5) == -5
    assert somar(1.9999, .0001) == 2
    assert somar(1,2,3,4,5,6,7,8,9) == 45
    assert somar((1, 2, 3, 4, 5, 6, 7, 8, 9)) == 45
    assert somar([1, 2, 3, 4, 5, 6, 7, 8, 9]) == 45


def test_str():
    assert somar('1', '9') == 10
    assert somar('1.1', '1.9') == 3
    assert somar('-10', '-5', '5.9', .1) == -9.0
    with pytest.raises(ValueError) as e:
        somar('a', 'b')
        e.match(r"could not convert string to float")
    with pytest.raises(ValueError) as e:
        somar([1, 2, 3, 4, 5, 6, 7, 8, 'a'])
        e.match(r"could not convert string to float")





if __name__ == '__main__':
    somar(1, 2)
    somar('1', '2')
    somar([1,2,3])